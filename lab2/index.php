<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>php</title>
    <link rel="stylesheet" href="../site_2/style.css">
    <link rel="stylesheet" href="/site_1/reseter.css">
</head>
<body>
    <div class="wrapper">
        <div class="wrapper-header">
            <header class="header">
                <img src="/site_1/images/logo.png" alt="logo" class="header__logo">
                <div >
                    <h1>
                        Лабораторная 2
                    </h1>
                </div>
            </header>
        </div>

        <main>
            <div class="wrapper-main">
                <form action="https://httpbin.org/post" method="post" class="form">
                    <fieldset class="form__personal-info">
                        <legend class="form__legend">Заполните поля ниже</legend> 
                        <div class="form__item">
                            <label>
                                Имя
                                <input type="text" name="name" placeholder="Ivan" class="form__input">
                            </label>
                        </div>

                        <div class="form__item">
                            <label class="form__label">
                                Email
                                <input type="email" name="email" placeholder="ivan@gmail.com" class="form__input">
                            </label>
                        </div>

                        <div class="form__item">
                            <p class="form__radio-button form__item" >
                                Отметьте тип обращения
                            </p>

                            <div class="form__radio form__item">
                                <input type="radio" name="обращение" value="жалоба" checked id="radio-appeal-1" class="radio__input">
                                <label for="radio-appeal-1" class="radio__label">
                                    жалоба
                                </label>
                            </div>

                            <div class="form__radio form__item">
                                <input type="radio" name="обращение" value="предложение" id="radio-appeal-2" class="radio__input">
                                <label for="radio-appeal-2" class="radio__label">
                                    предложение
                                </label>
                            </div>

                            <div class="form__radio form__item">
                                <input type="radio" name="обращение" value="благодарность" id="radio-appeal-3" class="radio__input">
                                <label for="radio-appeal-3" class="radio__label">
                                    благодарность
                                </label>
                            </div>

                            <div class="form__item">
                                <label class="form__label">
                                    <textarea type="text" name="текст" placeholder="напишите обращение" class="form__input"> </textarea>
                                </label>
                            </div>
                        </div> 

                        <p class="form__item">Отметьте удобный вариант ответа</p>

                        <div class="form__checkbox">
                            <input type="checkbox" name="feedback" value="sms" id="checkbox-feedback-1" class="checkbox__input">
                            <label for="checkbox-feedback-1" class="checkbox__label">
                                sms
                            </label>
                        </div>
                        
                        <div class="form__checkbox">
                            <input type="checkbox" name="feedback" value="email" id="checkbox-feedback-2" class="checkbox__input">
                            <label for="checkbox-feedback-2"  class="checkbox__label">
                                email
                            </label>
                        </div>

                    </fieldset>

                    <button type="submit" class="form__button">
                        Send
                    </button>

                    <div  class="form__button">
                        <a href="\site_2\page2.php">страница 2</a>
                    </div>

                </form>


                <?php

                ?> 
            </div>
        </main>

        <footer class="footer">
            <p class="footer__text">
            Собрать сайт из двух страниц.<br>
            1 страница: Сверстать форму обратной связи. Отправку формы осуществить на URL: https://httpbin.org/post. Добавить кнопку для перехода на 2 страницу.<br>
            2 страница: вывести на страницу результат работы функции get_headers. Загрузить код в удаленный репозиторий. Залить на хостинг.<br>

        </footer>
    </div>
</body>
</html>