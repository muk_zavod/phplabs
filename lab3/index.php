<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Лабораторная 3</h1>

    <?php
        $str = '4 * X = 36';
        $arr = explode(' ', $str);

        $i = 0;
        while ($i < count($arr)) {
            if ($arr[$i] == '*') {
                $operation = '*';
            } elseif ($arr[$i] == '-') {
                $operation = '-';
            } elseif ($arr[$i] == '+') {
                $operation = '+';
            } elseif ($arr[$i] == '/') {
                $operation = '/';
            }
            $i = $i + 1;
        }

        if ($operation == "-") {
            if ($arr[0] == 'X') {
                $answer = (float)$arr[2] + (float)$arr[4];
            } else {
                $answer = (float)$arr[0] - (float)$arr[4];
            }
        } elseif ($operation == "*") {
            if ($arr[0] == 'X') {
                $answer = (float)$arr[4] / (float)$arr[2];
            } else {
                $answer = (float)$arr[4] / (float)$arr[0];
            }
        } elseif ($operation == "+") {
            if ($arr[0] == 'X') {
                $answer = (float)$arr[4] - (float)$arr[2];
            } else {
                $answer = (float)$arr[4] - (float)$arr[0];
            }
        } elseif ($operation == "/") {
            if ($arr[0] == 'X') {
                $answer = (float)$arr[4] * (float)$arr[2];
            } else {
                $answer = (float)$arr[0] / (float)$arr[4];
            }
        }

        echo $answer;

    ?>

    <p>
        <?php
            print_r($answer)
        ?>
    </p>

</body>
</html>